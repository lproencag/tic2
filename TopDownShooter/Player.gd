extends KinematicBody

var speed = 5
var velocity = Vector3.ZERO


func _process(delta):
	velocity = Vector3()
	
	if Input.is_action_pressed("Right"):
		velocity.x += 1
	if Input.is_action_pressed("Left"):
		velocity.x -= 1
	if Input.is_action_pressed("Up"):
		velocity.z -= 1
	if Input.is_action_pressed("Down"):
		velocity.z += 1
		
	velocity = velocity.normalized() * speed
	
	move_and_slide(velocity)
