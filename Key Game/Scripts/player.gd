extends KinematicBody2D

export var speed = 150
var key_in_scene = false
var close_to_key = false
var close_to_door = false
onready var key = get_parent().get_node("key")
onready var door = get_parent().get_node("door")



func _ready():
	pass
	
func _process(delta):
	var dir = Vector2(0,0)
	if Input.is_action_pressed("Up"):
		dir.y = -1
	if Input.is_action_pressed("Down"):
		dir.y = 1
	if Input.is_action_pressed("Left"):
		dir.x = -1
	if Input.is_action_pressed("Right"):
		dir.x = 1
		
	move_and_slide(dir*speed)
	
func _input(event):
	if key_in_scene and close_to_key and Input.is_key_pressed(KEY_SPACE):
		key.queue_free()
		key_in_scene = false
	if close_to_door and close_to_key and Input.is_key_pressed(KEY_SPACE):
		door.queue_free()
	
func _on_Area2D_body_entered(body):
	if body.name == "key":
		close_to_key = true
		key_in_scene = true
	if body.name == "door":
		close_to_door = true


func _on_Area2D_body_exited(body):
	if body.name == "door":
		close_to_door = false
	
