extends Node2D

var pre_coin_head = preload("res://Scenes/CoinHead.tscn")
var pre_coin_tail = preload("res://Scenes/CoinTail.tscn")
var pre_enemy = preload("res://Scenes/Enemy.tscn")

func _ready():
	pass

func _process(delta):
		if Global.coin_head:
			instance_coin_head()
		if Global.coin_tail:
			instance_coin_tail()
		
func instance_coin_head():
	Global.coin_head = false
	var ch = pre_coin_head.instance()
	ch.position = Vector2(rand_range(250,765),rand_range(465,565))
	add_child(ch)
	print("Cara!")
	
func instance_coin_tail():
	Global.coin_tail = false
	var ct = pre_coin_tail.instance()
	ct.position = Vector2(rand_range(250,765),rand_range(465,565))
	add_child(ct)
	print("Coroa!")
