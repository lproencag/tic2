extends Control

onready var coin_display = $Label

func _process(delta):
	coin_display.text = "Coins: " + str(Global.coin)
	
func _on_ButtonEndTurn_button_down():
	if Global.player_turn:
		Global.emit_signal("end_turn")
		
func _on_ButtonBuyCard_button_down():
	if Global.player_turn and Global.coin >= 2:
		Global.coin -= 2
	else:
		 print("você não tem dinheiro ou não é sua vez")

func _on_ButtonFlipCoin_button_down():
	if Global.player_turn and Global.coin >= 1:
		Global.coin -= 1
		Global.flip_coin()
	else:
		 print("você não tem dinheiro ou não é sua vez")
