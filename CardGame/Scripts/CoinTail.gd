extends Area2D

onready var sprite = $Sprite
var is_selected = false

func _ready():
	pass
	
func _process(delta):
	if Global.player_turn == false:
		queue_free()
		
	if is_selected:
		set_position(get_viewport().get_mouse_position())
		
func _on_CoinTail_mouse_entered():
	sprite.scale = Vector2(1.1,1.1)
	Global.coin_tail_in = true
	Global.coin_head_in = false


func _on_CoinTail_mouse_exited():
	sprite.scale = Vector2(1,1)
	Global.coin_tail_in = false


func _on_CoinTail_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and Input.is_action_just_pressed("click"):
		on_click()
	elif event is InputEventMouseButton and Input.is_action_just_released("click"):
		is_selected = false
		
func on_click():
	is_selected = true
