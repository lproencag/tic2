extends Node2D

func _ready():
	start_player_turn()
	
	
func start_player_turn():
	Global.player_turn = true
	Global.coin += 20
	print("começou o turno do jogador")
	yield(Global,"end_turn")
	start_enemy_turn()
	
func start_enemy_turn():
	Global.player_turn = false
	print("começou o turno do inimigo")
	yield(Global,"end_turn")
	start_player_turn()

func _on_Timer_timeout():
	Global.emit_signal("end_turn")
	
