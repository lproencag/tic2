extends Node

#Variables
var coin = 0
var player_turn = false
var coin_head = false
var coin_tail = false
var coin_tail_in = false
var coin_head_in = false
var player_life

#Signals
signal end_turn 
signal flip_coin
signal coin_head
signal coin_tail

#func
func flip_coin():
	randomize()
	var coin_type = rand_range(0,1)
	if coin_type < 0.5:
		Global.coin_head = true
	else:
		Global.coin_tail = true
		
