==>Uma história narrada<==

Reviravolta e sensação de excitação do público

Ulfgar começou a sentir o cheiro de comida e apertou o passo em direção a taverna do velho de vidro. Depois de um longo dia entregando feno na cidade vizinha, sentia-se cansado da longa caminhada e estava sedento uma boa pratada de coelho, o único e o melhor prato do estabalecimento. Próximo a porta, um homem montado em um cavalo negro interrompe sua caminhada. Pela armadura exageradamente branca, o anão o reconhece como um clérigo de Alaris. Ainda montado, apresenta-se com polidez - Saudações, sou Edrik, Mensageiro dos novos arautos de Alaris. - Diz o homem, estendendo a mão aberta em direção a Ulfgar. A mão permanece estendida por pouco tempo, porém o suficiente para o homem relacionar o rosto emburrado do anão com a impossibilidade de suas mãos se tocarem. Constrangido, Edrik desce apressado do cavalo, retira de sua bolsa uma carta lacrada e a entrega para Ulfgar.

Logo em seguida, segue seu caminho rua abaixo. sua fome falava mais que a curiosidade, sem dizer que abrir cartas com dedos de grossos de anões não eram uma tarefa fácil.

 logo vem a lembrança da carta, depois de algumas tentativas ele a rasga ao meio e junta as duas partes, a carta contava sobre um grupo que lutava para proteger e ajudar os necessitados, o Poder da Luz sempre estaria lá quando alguém precisasse, não havia recompensa, apenas abrigo que valia muito naqueles tempos. Ulfgas já cansado de missões de entregador de comida, arrumou sua mochila e saiu da capela, a carta mandava ele seguir a estrada até ser interrompido por cavaleiros, ele entregaria a carta e se tornaria um membro, e assim foi feito, Ulfgar acreditava que Gollaf voltaria em ações como essas, acreditava na causa.

O anão havia entrado no inicio do grupo, eles ainda estavam arrumando o abrigo, no máximo 12 pessoas se encontravam lá e um deles era aquele mesmo clérigo que havia interceptado ele mas cedo, Edrik pede para ele descaçar, e diz que amanha teria muito trabalho para ele. Ulfgar então segue sua carreira de Herói do povo, ajudando com diversos problemas, bastava colocar uma vela ou uma lamparina em um lugar alto e logo o Poder da Luz chegava em seus cavalos para ascender e atender, e assim se foi durante 2 anos, o grupo cresceu e já contava com 50 homens, muita fama e homens se juntaram pela causa depois disso.

Ulfgar andava a noite com mais 5 homens, eles voltavam para o esconderijo depois de algumas missões, a noite parecia mais escura, alguns comentavam de um pressentimento ruim, mas Ulfgar dava de ombros como se fosse mais um dia chegando no esconderijo, sinais de batalha já apresentavam na entrada e dentro havia mais de trinta corpos mortes ao chão, havia apenas Edrik no centro ajoelhado e chorando, ele dizia que era culpa dele, eram todos inocentes e que ele havia colocado eles nessa luta, por isso era culpa dele, ao ver Ulfgar e seus companheiros, ele manda fugir antes que tenham o mesmo destino, as criaturas da noite mataram os membros do Poder da Luz, e elas iriam atras deles também, mandou eles embarcaram em um barco para Ilha de Sal o mais rápido possível antes sejam pegos. Ulfgar então fez, porem com a promessa de que iria voltar e

- Explica a situação do personagem principal
- Confronto
- Resultado do confronto
- Moral
- Fim



