https://savethecat.com/beat-sheets/

### IMAGEM DE ABERTURA:

  

(corresponde a 1ª página do roteiro)

É a imagem inicial do filme, o primeiro contato do espectador com a história que vai começar. Esta imagem irá definir o tom, o clima e o estilo do filme. Geralmente, o último beat (item 15) se relaciona diretamente com a imagem de abertura como um espelho, e a conexão entre elas cria um equilíbrio na história.

  

### DECLARAÇÃO DO TEMA:

  

(corresponde a 5ª página do roteiro)

Aproximadamente na página 5 do roteiro, ou seja, durante o set up (item 3), algum personagem faz uma declaração ou pergunta que define a premissa temática da obra, e que terá uma grande repercussão no final do filme. De início, o protagonista não a entende, pois, para isso, precisa passar por uma transformação. 



### SET UP:

  

(Páginas 1-10)

  

Em geral, as primeiras 10 páginas são utilizadas para a introdução do mundo e dos personagens. Este é o momento em que a situação inicial é apresentada, assim como as falhas, personalidades e motivações de todos os personagens principais, inclusive do vilão. Equivalente ao “mundo comum” na jornada do herói, o set up é extremamente importante, uma vez que é durante essas 10 primeiras páginas que você deve prender a atenção do espectador e cativá-lo com a obra. Blake Snyder chama este momento, e o primeiro ato de forma geral, de tese.

  

### O CATALISADOR:

  

(Página 12)

  

Também chamado de incidente inicial, ou incidente incitante, é o evento que dá o impulso inicial da trama, direcionando o protagonista ruma à sua aventura. 

  

### O DEBATE:

  

(Páginas 12-15)

  

Entre as páginas 12 e 15, mas se estendendo até o fim do primeiro ato (item 6). É possível relacionar o beat do debate aos beats da “recusa do chamado” e “consulta com o mentor” da jornada do herói. Ou seja, após o impulso inicial do catalisador, o protagonista pondera se deve ou não encarar essa aventura, de que forma, quais as consequências, e ao mesmo tempo receber conselhos e incentivos (ou não) dos demais personagens. O debate persiste até o protagonista tomar uma decisão.

  

### QUEBRA DO PRIMEIRO ATO:

  

(Página 25)

  

É quando o mundo comum do protagonista fica para trás após sua decisão, momento em que o protagonista inicia sua jornada em um mundo completamente diferente do que conhece. A quebra do primeiro ato é um importante momento de transição com o início da aventura. Blake Snyder se refere ao segundo ato como antítese.

  

### TRAMA PARALELA:

  

(Página 30)

  

A maioria das histórias tem diversos “plots”, e este é o momento de desenvolver o plot B do seu roteiro. Geralmente, o plot B é uma história de amor, mas não necessariamente um amor romântico, podendo ser uma relação de amizade, a própria relação de mentoria entre o protagonista e seu mentor, ou alguma relação familiar. Também é o momento de introdução de novos personagens e conflitos, se for o caso.

  

### JOGOS E DIVERSÃO:

  

(Páginas 30-35)

  

Como o próprio nome deste beat sugere, é hora de explorar todas aquelas ideias de ação e aventura propriamente ditas. Aqui, teremos o protagonista explorando seu novo mundo, e entretendo o público. É a promessa da premissa, segundo Snyder, o cartaz do seu filme. A depender do gênero da sua história, este será o momento, por exemplo, dos enigmas, investigações e intrigas, a aventura em si, como o treinamento de Neo em Matrix.

  

### MIDPOINT:

  

(Página 55)

  

O midpoint marca a divisão das duas partes de um filme. É o momento em que sua história e a aventura do protagonista mudam de direção: se tudo estava fácil, agora passa a ser difícil, e as dificuldades e problemas do protagonista se tornam sérias e urgentes. No midpoint, acontece uma das seguintes coisas: ou o protagonista perde tudo o que tinha (falsa derrota), ou ganha tudo aquilo que acredita desejar (falsa vitória). Mais do que isso, é o momento em que um prazo é estipulado (o relógio começa a correr), as apostas e riscos são mais altos, e há o beijo entre o casal principal.

  

### VILÕES FECHAM O CERCO:

  

(Páginas 55-75)

  

Este é o ponto do roteiro no qual os vilões atacam o protagonista com mais força. Na maioria das tramas, não só os vilões caem matando em cima do herói, como ele próprio pode vir a se prejudicar de alguma forma. Além disso, é o momento em que questões internas, dúvidas e divergências, acabam desintegrando o grupo de aliados do próprio herói.

  

### TUDO ESTÁ PERDIDO:

  

(Página 75)

  

“E agora? Como ele vai sair dessa?” É esta a sensação que o espectador deve ter nesse beat. De acordo com Snyder, é o momento da “morte”. Aqui, o casal se separa, o herói é capturado, alguém é exposto ou cai em uma armadilha. Tudo isso culmina no ápice das coisas negativas que poderiam acontecer e, é neste momento em que a forma como o protagonista pensava durante todo o filme muda, ocorre a morte de ideias velhas. É o momento de transformação de ideais que ajudará no renascimento do protagonista, agora transformado.

  

### A NOITE ESCURA DA ALMA:

  

(Páginas 75-85)

  

“Agora que estou morto, o que faço sobre isso?” O protagonista ainda está decidindo quem é e o que fará a seguir, está lamentando tudo que perdeu e o que isso significa. A cena pode ter 5 segundos ou 5 minutos, mas é importante para que o tema do filme seja retomado, pois agora o protagonista entende aquela declaração inicial que antes não fazia sentido (item 2). Este é o momento em que o protagonista precisa encontrar forças para a superação.

  

### QUEBRA DO SEGUNDO ATO:

  

(Página 85)

  

Início do que Blake Snyder chama de síntese. Em razão de todos os acontecimentos até aqui, surge uma solução que costuma partir do plot B. Ou seja, é o momento no qual os plots se encontram e a trama paralela (plot B) ajuda na solução da trama principal (plot A). Snyder costumava dizer que aqui o protagonista “acorda” e agora sabe o que deve fazer, falta apenas colocar o plano em ação.

  

### FINALE:

  

(Páginas 85-110)

  

O plano é colocado em prática, assim como todas as lições aprendidas pelo protagonista no caminho. Nestas últimas páginas, encontramos o clímax do filme e a batalha principal. É durante este finale que toda a situação apresentada anteriormente é resolvida, os vilões são derrotados, o protagonista conquista o coração da garota e passa por sua evolução final, construindo um novo mundo. Ou seja, na beat sheet proposta por Snyder, ao contrário do que pode parecer em razão do nome, finale não é o momento final do filme, ou a última cena que vemos, e sim todo o processo de resolução da história.

  

### IMAGEM FINAL:

  

(Página 110)

  

Conforme mencionado na imagem inicial (item 1), a imagem final é seu espelho, traduzindo o exato oposto do que aquela primeira imagem mostrava. Agora, o protagonista já passou pela transformação durante a história e, consequentemente, seu mundo e cotidiano também. A imagem final é tão importante quanto a inicial, pois, enquanto a inicial é a primeira impressão do seu espectador, a final é a última, representando o final tanto da história como do arco do seu protagonista. 

  

Em geral, a beat sheet é feita depois do argumento e antes da escaleta. Muitos roteiristas utilizam a estrutura proposta por Snyder, mas vale sempre lembrar que não há regra. A melhor forma de escrever varia dependendo do processo criativo de cada um e, muitas vezes, do que faz mais sentido para a obra. O importante é conhecer as ferramentas, como esta, para saber como utilizá-la e descobrir o seu próprio processo!

113131
