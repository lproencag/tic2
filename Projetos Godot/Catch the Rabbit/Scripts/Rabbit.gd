extends Sprite

var pos_start
export var pos_final = 0
var up = true
export var speed = 0

func _ready():
	pos_start = position.y
	pass

func _process(delta):
	aceleration()

func _on_Rabbit_Button_button_down():
	queue_free()


func aceleration():

	if position.y < pos_start - pos_final:
		position.y += speed
		up = false
		
	if up:
		position.y -= speed
	else:
		position.y += speed * 0.5
