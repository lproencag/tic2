extends Node2D

var pre_rabbit = preload("res://Scenes/Rabbit.tscn")
var spawn_delay = 0

func _ready():
	pass

func _process(delta):
	delay(delta)
	
func delay(delta):
	spawn_delay += delta
	if spawn_delay > rand_range(1.5,2):
		instance_rabbit()
		spawn_delay = 0

func instance_rabbit():
	var rabbit = pre_rabbit.instance()
	rabbit.position = Vector2(rand_range(40,560),891)
	rabbit.speed = rand_range(0.3,0.5)
	rabbit.pos_final = rand_range(500,800)
	add_child(rabbit)
	
