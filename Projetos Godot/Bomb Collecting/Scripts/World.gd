extends Node2D

var pre_bomb = preload("res://Scenes/Bomb.tscn")
var time = 0
export var delay = 1

#Points System
var points = 0
onready var txt_label = $Label

func _process(delta):
	
	txt_label.text = "Points: " + str(points)
	
	time += delta
	if time > delay:
		BombSpawn()
		time = 0
		
func BombSpawn():
	var bomb = pre_bomb.instance()
	bomb.position = Vector2(rand_range(30,500),0)
	self.add_child(bomb)
	
