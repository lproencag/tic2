extends KinematicBody2D

export var speed = 300
onready var spr_player = $Sprite

func _ready():
	pass

#Movement System
func _process(delta):
	var dir = Vector2()
	if Input.is_action_pressed("Left"):
		spr_player.set_flip_h(true)
		$Collider.position.x = -23.609
		dir.x = -1
		
	if Input.is_action_pressed("Right"):
		spr_player.set_flip_h(false)
		$Collider.position.x = 23.609
		dir.x = 1	
		
#Collider System
	if self.position.x < 62:
		self.position.x = 62
	if self.position.x > 450:
		self.position.x = 450
		
	move_and_slide(dir * speed)
