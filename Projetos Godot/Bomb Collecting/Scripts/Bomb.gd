extends Area2D

export var speed = 200

var pre_smoke = preload("res://Scenes/Smoke.tscn")
var t_smoke = 0

var pre_contact_smoke = preload("res://Scenes/Contact_smoke.tscn")
var pre_contact_explosion = preload("res://Scenes/Contact_Explosion.tscn")

func _ready():
	pass
	
func _process(delta):
	speed += 1
	self.position.y += speed * delta
	if self.position.y > 700:
		queue_free()
		
	t_smoke += delta
	if t_smoke > rand_range(0.1, 0.05):
		smoke()
		t_smoke = 0
		


func _on_Bomb_body_entered(body):
	if body.name == "Player":
		get_parent().points += 10
		contact_smoke()
		queue_free()
	
	
func _on_Bomb_area_entered(area):
	if area.name == "Ground":
		get_parent().points -= 10
		contact_explosion()
		queue_free()
		
func smoke():
	var smoke = pre_smoke.instance()
	smoke.position = position + Vector2(rand_range(-15,-30),-20)
	get_parent().add_child(smoke)

func contact_explosion():
	var explosion = pre_contact_explosion.instance()
	explosion.position = position
	get_parent().add_child(explosion)
	
func contact_smoke():
	var smoke = pre_contact_smoke.instance()
	smoke.position = position
	get_parent().add_child(smoke)
	
